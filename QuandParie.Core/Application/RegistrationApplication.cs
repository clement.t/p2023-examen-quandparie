﻿using QuandParie.Core.Domain;
using QuandParie.Core.Services;
using QuandParie.Core.Persistance;
using System.Threading.Tasks;
using QuandParie.Core.ReadOnlyInterfaces;

namespace QuandParie.Core.Application
{
    public class RegistrationApplication
    {
        private readonly ICustomerRepository _customerRepository;
        private readonly IDocumentRepository _documentRepository;
        private readonly IIdentityProofer _identityProofer;
        private readonly IAddressProofer _addressProofer;

        public RegistrationApplication(
            ICustomerRepository customerRepository, 
            IDocumentRepository documentRepository, 
            IIdentityProofer identityProofer,
            IAddressProofer addressProofer)
        {
            _customerRepository = customerRepository;
            this._documentRepository = documentRepository;
            this._identityProofer = identityProofer;
            this._addressProofer = addressProofer;
        }

        public async Task<IReadOnlyCustomer> CreateAccount(string email, string firstName, string lastName)
        {
            var customer = new Customer(email, firstName, lastName);
            await _customerRepository.SaveAsync(customer);

            return customer;
        }

        public async Task<IReadOnlyCustomer> GetAccount(string email)
        {
            return await _customerRepository.GetAsync(email);
        }

        public async Task<bool> UploadIdentityProof(string email byte[] proof)
        {
            var customer = await _customerRepository.GetAsync(email);
            if (!_identityProofer.Validates(customer, proof))
                return false;

            await _documentRepository.SaveAsync(DocumentType.IdentityProof, customer, proof);

            customer.IsIdentityVerified = true;
            await _customerRepository.SaveAsync(customer);

            return true;
        }

        public async Task<bool> UploadAddressProof(string email, byte[] proof)
        {
            var customer = await _customerRepository.GetAsync(email);
            if (!_addressProofer.Validates(_customerRepository, out var address , proof))
                return false;

            await _identityProofer.SaveAsync(DocumentType.AddressProof, email, proof);

            customer.Address = address;
            await _customerRepository.SaveAsync(customer);

            return true;
        }
    }
}
